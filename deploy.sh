#!/bin/bash

echo "Deploying cpuload kubelet from online yaml file based on docker image docker.io/iaroslavplutenko/cpuload:latest"
result=$(kubectl create -f https://bitbucket.org/iaroslavplutenko/cpuload/raw/656161b6a06af72ac0282a8e01600d423e0752fa/cpuload-deploy.yaml)
echo $result

if [[ "$result" == "deployment.apps/cpuload-app created" ]]; then
    echo "Exposing kubelet to outer world - creating a service"
    result=$(kubectl expose deployment cpuload-app --name=cpuload-app --type=LoadBalancer --target-port=4567)
    echo $result
    if [[ "$result" == "service/cpuload-app exposed" ]]; then
        service=$(minikube service cpuload-app --url)
        echo "Your service is available at $service"
        echo "You can use services:"
        echo "${service}/hello"
        echo "${service}/healthcheck"
        echo "${service}/workload?duration=20"
        echo "${service}/ddos?duration=15"
    fi
else
    echo "Script execution aborted"
fi


