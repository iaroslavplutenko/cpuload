#!/bin/bash
echo "Sending a bunch of http requests to cublelet causing hi cpu load and denial of service"
service=$(minikube service cpuload-app --url)
threshold=200
i=0
while [ $i -lt $threshold ]
do
    curl ${service}/workload?duration=5   
    #sleep .5
done
echo "Completed"
