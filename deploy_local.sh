#!/bin/bash
minikube docker-env #- needed for minikube docker daemon to access host system docker images
echo "Deploying cpuload kubelet from local yaml file based on docker cpuload:latest"
result=$(kubectl create -f cpuload-deploy-local.yaml)
echo $result

if [[ "$result" == "deployment.apps/cpuload-app created" ]]; then
    echo "Exposing kubelet to outer world - creating a service"
    result=$(kubectl expose deployment cpuload-app --name=cpuload-app --type=LoadBalancer --target-port=4567)
    echo $result
    if [[ "$result" == "service/cpuload-app exposed" ]]; then
        service=$(minikube service cpuload-app --url)
        echo "Your service is available at $service"
        echo "You can use services:"
        echo "${service}/hello"
        echo "${service}/healthcheck"
        echo "${service}/workload?duration=20"
        echo "${service}/ddos?duration=15"
    fi
else
    echo "Script execution aborted"
fi


