package com.ucu.plutenko;

import static java.lang.Math.*;
import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
        get("/hello", (req, res) -> "Hello my World");
        get("/healthcheck", (req, res) -> "OK");
        get("/workload", (req, res) -> workload(req.queryParams("duration")));
        get("/ddos", (req, res) -> ddos(req.queryParams("duration")));
    }

    static String workload(String duration) {
        try
        {
            // the String to int conversion happens here
            Long dur = Long.parseLong(duration.trim()) *1000 ;

            // print out the value after the conversion
            //System.out.println("Long dur = " + dur);

            int numCore = 2;
            int numThreadsPerCore = 2;
            double load = 0.95;
            for (int thread = 0; thread < numCore * numThreadsPerCore; thread++) {
                new BusyThread("Thread" + thread, load, dur).start();
            }
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("NumberFormatException: " + nfe.getMessage());
            return "Unknown duration format";
        }
        return "CPU Load started for " + duration + " sec";
    }

    static String ddos(String duration) {
        try
        {
            // the String to int conversion happens here
            Long dur = Long.parseLong(duration.trim()) *1000 ;
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime < dur) {
                for (int i=0; i<1000; i++) {
                    double d = tan(atan(tan(atan(tan(atan(tan(atan(tan(atan(123456789.123456789))))))))));
                    cbrt(d);
                }
            }
        }
        catch (NumberFormatException  nfe)
        {
            System.out.println("NumberFormatException: " + nfe.getMessage());
            return "Unknown duration format";
        }
        return "Return from heavy load after " + duration + " sec";
    }

    /*
    static String hicpu(String duration) {
        try
        {
            // the String to int conversion happens here
            Long dur = Long.parseLong(duration.trim()) *1000 ;

            // print out the value after the conversion
            //System.out.println("Long dur = " + dur);

            long startTime = System.currentTimeMillis();
            // Loop for the given duration
            while (System.currentTimeMillis() - startTime < dur) {
                Math.sqrt(Math.exp(13) + 1249*Math.sqrt(2435234));

            }
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("NumberFormatException: " + nfe.getMessage());
            return "Unknown duration format";
        }
        return "CPU Load finished after " + duration + " sec";
    }
*/

}